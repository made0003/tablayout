package examples.quickprogrammingtips.com.tablayout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;

//test line
@SuppressLint("SetJavaScriptEnabled")

public class DataFragment extends Fragment {

    WebView webView;
    public int num1, num2;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Make sure that container activity implement the callback interface
        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View view =  inflater.inflate(R.layout.fragment_data, container, false);

            // Suppose that when a button clicked second FragmentB will be inflated
            // some data on FragmentA will pass FragmentB
            Button dataButton = (Button) view.findViewById(R.id.doSomethingButton);
            return view;

            num1 = 12;
            num2 = 34;

            webView = (WebView) getView().findViewById(R.id.webView);
            webView.addJavascriptInterface(new WebAppInterface(), "Android");

            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl("file:///android_asset/chart.html");

        }
    public class WebAppInterface {

        @JavascriptInterface
        public int getNum1() {
            return num1;
        }

        @JavascriptInterface
        public int getNum2() {
            return num2;
        }


    }

}
