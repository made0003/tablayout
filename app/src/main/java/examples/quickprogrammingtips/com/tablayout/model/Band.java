package examples.quickprogrammingtips.com.tablayout.model;

/**
 * Created by anton on 8-1-16.
 * A Band has one or more Albums
 */
public class Band extends Performer {
    public String members;

    public void setMembers(String name, String members) {
        this.members = members;
    }

    public String getMembers() {
        return members;
    }

    public Band() {
    }

    /**
     *
     * @param name: name of band
     */
    public Band(String name){
        this.setName(name);
    }
}
