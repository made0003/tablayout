package examples.quickprogrammingtips.com.tablayout;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import examples.quickprogrammingtips.com.tablayout.model.Album;
import examples.quickprogrammingtips.com.tablayout.model.Artist;
import examples.quickprogrammingtips.com.tablayout.model.Band;
import examples.quickprogrammingtips.com.tablayout.model.Performer;


public class MainActivity extends AppCompatActivity implements  OnTaskCompleted{

    private String author;
    private String programName;
    private static final String CARD_API = "http://46.105.120.168/cardapi/";
    private final OnTaskCompleted onTaskCompleted = this;//enables call-back methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_main);
        TabLayout tabLayout=(TabLayout)findViewById(R.id.tabLayout);
        tabLayout.setTabTextColors(Color.WHITE, R.color.accent_material_dark);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.addTab(tabLayout.newTab().setText("Data"));
        tabLayout.addTab(tabLayout.newTab().setText("Albums"));
        tabLayout.addTab(tabLayout.newTab().setText("Artists"));
        tabLayout.addTab(tabLayout.newTab().setText("Select"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#00FFFF"));

        //test-array of albums
        ArrayList<Album> albums = new ArrayList<>(Arrays.asList(
                new Album(new Band("Beatles"),"Please Please Me (1963)"),
                new Album(new Band("Beatles"),"With The Beatles (1963)"),
                new Album(new Band("Beatles"),"A Hard Day's Night (1964)"),
                new Album(new Band("Beatles"),"Beatles For Sale (1964)"),
                new Album(new Band("Beatles"),"Help (1965)"),
                new Album(new Band("Beatles"),"Rubber Soul (1965)"),
                new Album(new Band("Beatles"),"Revolver (1966)"),
                new Album(new Band("Beatles"),"Sgt. Pepper's Lonely Hearts Club Band (1967)"),
                new Album(new Band("Beatles"),"Magical Mystery Tour (1967)"),
                new Album(new Band("Beatles"),"1The Beatles (1968)"),
                new Album(new Band("Beatles"),"Yellow Submarine (1969)"),
                new Album(new Band("Beatles"),"Abbey Road (1969)"),
                new Album(new Band("Beatles"),"Let It Be"),
                new Album(new Artist("David Bowie"),"Hunky Dory"),
                new Album(new Artist("David Bowie"),"The Rise And Fall Of Ziggy Stardust And The Spiders From Mars"),
                new Album(new Artist("David Bowie"),"Aladdin Sane"),
                new Album(new Artist("David Bowie"),"Pin Ups"),
                new Album(new Artist("David Bowie"), "Diamond Dogs"),
                new Album(new Artist("David Bowie"),"Young Americans"),
                new Album(new Artist("David Bowie"),"Station To Station"),
                new Album(new Artist("David Bowie"),"Heroes"),
                new Album(new Artist("David Bowie"),"Low"),
                new Album(new Artist("David Bowie"),"Stage"),
                new Album(new Artist("David Bowie"),"Lodger"),
                new Album(new Artist("David Bowie"),"Scary Monsters"),
                new Album(new Artist("David Bowie"), "Let's Dance"),
                new Album(new Artist("David Bowie"),"Tonight"))
        );

        //test-array of artists
        ArrayList<Performer> artists = new ArrayList<>(Arrays.asList(
                new Band("Bad Company"),
                new Band("Band"),
                new Band("Barclay James Harvest"),
                new Artist("Peter Bardens"),
                new Band("Be-Bop Deluxe"),
                new Band("Beach Boys"),
                new Band("Beatles"),
                new Band("Beethoven"),
                new Artist("Bill Bruford"),
                new Band("Bintangs"),
                new Artist("Bjork"),
                new Band("Black Keys"),
                new Band("Black Uhuru"),
                new Band("Blind Faith"),
                new Band("Blondie"),
                new Band("Blood, Sweat&Tears"),
                new Band("Booker T and the MG's"),
                new Band("Bootsy's Rubber Band"),
                new Band("Brainbox"),
                new Band("Bread"),
                new Band("Buena Vista Social Club"),
                new Band("Byrds"))
        );

        //initialise variables to use
        author="@string/author";
        programName="Muziek";

        Album revolver=new Album();
        revolver.setTitle("Revolver");

        //display tablayout
        displayHome();
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition()==0)
                    displayHome();
                if (tab.getPosition()==1)
                    getSupportFragmentManager().beginTransaction().replace(R.id.frLayout, new AlbumsFragment()).commit();
                if (tab.getPosition()==2)
                    getSupportFragmentManager().beginTransaction().replace(R.id.frLayout, new ArtistsFragment()).commit();
                if (tab.getPosition()==3)
                    getSupportFragmentManager().beginTransaction().replace(R.id.frLayout, new SelectFragment()).commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        Toolbar tool = (Toolbar)findViewById(R.id.app_bar);//cast it to ToolBar
        setSupportActionBar(tool);
        ImageButton playButton = (ImageButton) tool.findViewById(R.id.playButton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "play button", Toast.LENGTH_LONG).show();
            }
        });
        ImageButton stopButton = (ImageButton) tool.findViewById(R.id.stopButton);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "stop button", Toast.LENGTH_LONG).show();
            }
        });
        ImageButton pauseButton = (ImageButton) tool.findViewById(R.id.pauseButton);
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "pause button", Toast.LENGTH_LONG).show();
            }
        });
        ImageButton forwardButton = (ImageButton) tool.findViewById(R.id.forwardButton);
        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "forward button", Toast.LENGTH_LONG).show();
            }
        });
        ImageButton backButton = (ImageButton) tool.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "back button", Toast.LENGTH_LONG).show();
            }
        });

    }

    public boolean areCrashesEnabled() {
        SharedPreferences preferences;
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return preferences.getBoolean("are_crashes_enabled", false);
    }
    private void displayHome() {
        getSupportFragmentManager().beginTransaction().replace(R.id.frLayout,new DataFragment()).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String result, String call) {

    }
}
